FROM ruby:3.2.0-bullseye

ENV WORKDIR=/usr/src/app

WORKDIR $WORKDIR

ARG RAILS_ENV=production
ARG RACK_ENV=production

ENV RAILS_ENV ${RAILS_ENV}
ENV RACK_ENV ${RACK_ENV}

ADD ci ./ci

RUN ./ci/scripts/system-deps.sh

COPY Gemfile Gemfile.lock .ruby-version ./

RUN ./ci/scripts/install-gems.sh

COPY config ./config

COPY app/assets ./app/assets

COPY Rakefile ./

RUN bundle exec rake assets:precompile

COPY . ./

HEALTHCHECK CMD curl -f http://localhost/

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 80

ARG GIT_COMMIT_SHA

LABEL GIT_COMMIT_SHA=${GIT_COMMIT_SHA}

RUN setcap 'cap_net_bind_service=+ep' /usr/sbin/nginx

RUN chown -R www-data:www-data run/ log/ tmp/

USER www-data

CMD ["foreman", "start"]
