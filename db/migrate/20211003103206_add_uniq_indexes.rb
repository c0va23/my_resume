# frozen_string_literal: true

class AddUniqIndexes < ActiveRecord::Migration[5.2]
  def change
    add_index :tools, :name, unique: true
    add_index :tool_projects, %i[project_id tool_id], unique: true
    add_index :tool_types, :name, unique: true
  end
end
