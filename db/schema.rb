# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2021_10_03_103206) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "site_url"
    t.datetime "started_at", precision: nil, null: false
    t.datetime "ended_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["name"], name: "index_companies_on_name", unique: true
  end

  create_table "contacts", id: :serial, force: :cascade do |t|
    t.string "label", null: false
    t.string "url", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "value"
    t.index ["url"], name: "index_contacts_on_url", unique: true
  end

  create_table "pages", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "projects", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "url"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.text "role"
    t.integer "company_id"
    t.index ["company_id"], name: "index_projects_on_company_id"
  end

  create_table "screenshots", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "image"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "project_id"
  end

  create_table "time_slots", id: :serial, force: :cascade do |t|
    t.date "started_at"
    t.date "ended_at"
    t.integer "project_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["project_id"], name: "index_time_slots_on_project_id"
  end

  create_table "tool_projects", id: :serial, force: :cascade do |t|
    t.integer "project_id"
    t.integer "tool_id"
    t.string "version"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["project_id", "tool_id"], name: "index_tool_projects_on_project_id_and_tool_id", unique: true
  end

  create_table "tool_types", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.index ["name"], name: "index_tool_types_on_name", unique: true
  end

  create_table "tools", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "tool_type_id"
    t.index ["name"], name: "index_tools_on_name", unique: true
    t.index ["tool_type_id"], name: "index_tools_on_tool_type_id"
  end

  add_foreign_key "projects", "companies"
end
