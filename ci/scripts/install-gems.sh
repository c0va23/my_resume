#!/usr/bin/env sh

set -ex

gem update --system

bundle config build.nokogiri --use-system-libraries
bundle config list

bundle install
