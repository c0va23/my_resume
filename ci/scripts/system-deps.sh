#!/usr/bin/env sh

set -ex

apt update
apt upgrade -y
apt install -y \
  ruby-dev \
  build-essential \
  libxml2-dev \
  libxslt-dev \
  libpq-dev \
  nodejs \
  curl \
  nginx

if test "${RAILS_ENV}" != "production"; then
  apt install -y \
    libsqlite3-0 \
    libsqlite3-dev \
    sqlite3 \
    git
fi

if test "${RAILS_ENV}" = "production"; then
  rm -rf /var/lib/apt/lists/*
fi
