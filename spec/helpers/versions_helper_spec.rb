# frozen_string_literal: true

require 'spec_helper'

describe VersionsHelper do
  describe '#render_version_graphic' do
    before do
      assign(:versions, versions)
      # rubocop:disable Rails/RenderInline:
      render inline: '<% render_version_graphic @versions %>'
      # rubocop:enable Rails/RenderInline:
    end

    context 'with one version' do
      let(:versions) { [Tool::Version.new('2.0', 2.years)] }

      it 'render ellipse' do
        expect(rendered).to eq '<ellipse fill="#70142f" cx="50.0" cy="50.0" rx="50.0" ry="50.0" />'
      end
    end

    context 'with two equal version' do
      let(:versions) do
        [
          Tool::Version.new('2.5', 1.year),
          Tool::Version.new('2.6', 1.year),
        ]
      end

      it 'render two arc' do
        expect(rendered).to eq \
          '<path fill="#555a5c" d="M 50.0 50.0 L 100.0 50.0 A 50.0 50.0 0 0,1 100.0 50.0 Z" />' \
          '<path fill="#e21c87" d="M 50.0 50.0 L 100.0 50.0 A 50.0 50.0 0 0,1 100.0 50.0 Z" />'
      end
    end

    context 'with three diff version' do
      let(:versions) do
        [
          Tool::Version.new('2.2', 3.years),
          Tool::Version.new('2.3', 2.years),
          Tool::Version.new('2.4', 1.year),
        ]
      end

      it 'render two arc' do
        expect(rendered).to eq \
          '<path fill="#d0d5f5" d="M 50.0 50.0 L 100.0 50.0 A 50.0 50.0 0 0,1 100.0 50.0 Z" />' \
          '<path fill="#ce0d88" d="M 50.0 50.0 L 100.0 50.0 A 50.0 50.0 0 0,1 100.0 50.0 Z" />' \
          '<path fill="#98a16a" d="M 50.0 50.0 L 100.0 50.0 A 50.0 50.0 0 0,1 100.0 50.0 Z" />'
      end
    end
  end
end
