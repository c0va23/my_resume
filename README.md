### README

Resume for one developer.

## Tools

* rails 5.2
* ruby 2.6

## Environment variables

* `ADMIN_USERNAME` — login for administration panel.
* `ADMIN_PASSWORD` ­— password for administration panel.
* `CLOUDINARY_URL` — configuration url for Cloudinary (start with "cloudinary://").
* `SECRET_KEY_BASE` — need only for production environment (see file `config/secret.rb`).
* `YANDEX_METRIKA_COUNTER_ID` — Yandex.Metrika counter ID.
* `ROLLBAR_ACCESS_TOKEN` — access token for Rollbar.

## Demo

http://resume.fedorenko-d.ru/
