# frozen_string_literal: true

module VersionsHelper
  WIDTH = HEIGH = 100.0
  RADIUS_X = WIDTH / 2
  RADIUS_Y = HEIGH / 2
  RADIUS_COORDS = [RADIUS_X, RADIUS_Y].freeze
  CENTER_X = RADIUS_X
  CENTER_Y = RADIUS_Y
  CENTER_COORDS = RADIUS_COORDS
  DEG_IN_PI = 180
  ROUND_COOR = 2

  def render_version_graphic(versions)
    version_parts(versions, versions.sum(&:period)).each do |(start, end_, color)|
      start_engel = start * DEG_IN_PI * 2
      end_angel = end_ * DEG_IN_PI * 2
      render_version_pie start_engel, end_angel, fill: color
    end
  end

  def render_version_pie(start_angel_deg, end_angel_deg, **options)
    if (end_angel_deg - start_angel_deg) >= (DEG_IN_PI * 2)
      render_circle(**options)
    else
      sector = Sector.new(start_angel_deg, end_angel_deg)
      render_sector(sector, options)
    end
  end

  protected

  Sector = Struct.new(:start_deg, :end_deg)

  def version_parts(versions, period_total)
    end_ = 0
    versions.map do |version|
      start = end_
      end_ = start + (version.period / period_total)
      [start, end_, version.color]
    end
  end

  def deg_to_rad(deg)
    (DEG_IN_PI**-1) * Math::PI * deg
  end

  def coords(angel_deg)
    angel_rad = deg_to_rad(angel_deg)
    [
      coor_x(angel_rad),
      coor_y(angel_rad)
    ]
  end

  def coor_x(angel_rad)
    (CENTER_X + (Math.cos(angel_rad) * RADIUS_X)).round(ROUND_COOR)
  end

  def coor_y(angel_rad)
    (CENTER_Y + (Math.sin(angel_rad) * RADIUS_Y)).round(ROUND_COOR)
  end

  def move_to(coords)
    x, y = coords
    "M #{x} #{y}"
  end

  def line_to(coords)
    x, y = coords
    "L #{x} #{y}"
  end

  def arc_to(radius_coords, angel, large_arc, sweep_arc, end_coords)
    radius_x, radius_y = radius_coords
    end_x, end_y = end_coords
    "A #{radius_x} #{radius_y} #{angel} #{large_arc},#{sweep_arc} #{end_x} #{end_y}"
  end

  def close_path
    'Z'
  end

  def render_circle(options)
    options = options.merge(cx: CENTER_X, cy: CENTER_Y, rx: RADIUS_X, ry: RADIUS_Y)
    concat(tag.ellipse(**options))
  end

  def large_and_sweep_arc(sector)
    deg_diff = sector.end_deg - sector.start_deg

    large_arc = deg_diff > DEG_IN_PI ? 1 : 0
    sweep_arc = deg_diff.positive? ? 0 : 1

    [large_arc, sweep_arc]
  end

  def render_sector(sector, options)
    points = section_points(sector)

    options = options.merge(d: points)

    concat(tag.path(**options))
  end

  def section_points(sector)
    start_coords = coords(sector.start_deg)
    end_coords = coords(sector.end_deg)

    large_arc, sweep_arc = large_and_sweep_arc(sector)

    [
      move_to(CENTER_COORDS),
      line_to(start_coords),
      arc_to(RADIUS_COORDS, 0, large_arc, sweep_arc, end_coords),
      close_path,
    ].join(' ')
  end
end
