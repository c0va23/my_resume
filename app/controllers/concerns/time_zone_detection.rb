# frozen_string_literal: true

module TimeZoneDetection
  extend ActiveSupport::Concern

  included do
    around_action :with_time_zone
  end

  protected

  def with_time_zone(&)
    time_zone = request.headers['Time-Zone']
    return yield if time_zone.blank?

    Time.use_zone(time_zone.to_i, &)
  end
end
