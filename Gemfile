# frozen_string_literal: true

source 'https://rubygems.org'

ruby File.read('.ruby-version').strip

# rubocop:disable Metrics/BlockLength
source 'https://rubygems.org' do
  rails_version = '~> 7.0.4'
  gem 'actionpack', rails_version
  gem 'actionview', rails_version
  gem 'activerecord', rails_version
  gem 'activesupport', rails_version

  gem 'sprockets-rails'

  gem 'rails-i18n'

  gem 'tzinfo-data'

  # DB
  gem 'pg', '~> 1.1'

  # View
  gem 'redcarpet', '~> 3.5.1'
  gem 'representable', '~> 3.1.1'
  gem 'responders', '~> 3.0.1'
  gem 'simple-navigation', '~> 4.0'
  gem 'slim', '~> 4.1.0'

  gem 'multi_json' # required by representable

  # Image
  gem 'carrierwave', '~> 1.2.2'
  gem 'cloudinary', '~> 1.21.0'

  # Validation
  gem 'date_validator', '~> 0.9'

  # Assets
  gem 'coffee-rails', '~> 5.0.0'
  gem 'sass-rails', '~> 6.0.0'
  gem 'uglifier', '>= 1.3.0'

  gem 'bootstrap-sass', '~> 3.4'
  gem 'jquery-rails'

  # Server
  gem 'puma', '~> 6.0'

  # Error handling
  gem 'rollbar', '~> 3.2.0'

  group :development do
    gem 'byebug'
    gem 'flog', require: false
    gem 'guard-rspec'
    gem 'guard-rubocop'
    gem 'i18n-tasks'
    gem 'solargraph', '0.44.0', require: false
  end

  group :test, :development do
    gem 'brakeman', require: false
    gem 'factory_bot_rails'
    gem 'ffaker'
    gem 'reek', '~> 6.1.3', require: false
    gem 'rspec-its'
    gem 'rspec-rails', '~> 5.0.2', require: false
    # rubocop 1.43 requer ruby_parser 3.2.0, not supported by reek
    gem 'rubocop', '~> 1.43.0', require: false
    gem 'rubocop-performance', '~> 1.15.2', require: false
    gem 'rubocop-rails', '~> 2.17.4', require: false
    gem 'rubocop-rspec', '~> 2.17.0', require: false
    gem 'shoulda-matchers', '~> 5.0.0', require: false
    gem 'simplecov'
    gem 'sqlite3', '~> 1.4.2'
    gem 'timecop'
  end

  group :production do
    gem 'foreman', require: false
    gem 'rails_12factor'
  end
end
# rubocop:enable Metrics/BlockLength

source 'https://rails-assets.org' do
  gem 'rails-assets-bootstrap-markdown', '~> 2.5'
  gem 'rails-assets-marked', '~> 0.3.2'
  gem 'rails-assets-vis', '~> 3.12'
end
